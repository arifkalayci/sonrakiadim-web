(function() {
"use strict";

var appId = "2p76Z6qzHCAsP6X2eD4WaOohijXXUHe4xAhICHQy";
var jsKey = "o7xnpWNaD4o2m8m9SDElt7C6lwI5SqjqkCcNedGz";
var restAPIKey = "9rPPNGpbrIkifDehm1tbeYwzL0DQWX1qI4acplXC";

//Parse.Object.toJSON() does not convert nested Parse.Object's
Parse.Object.prototype.toFullJSON = function() {
    var attributes = _.clone(this.attributes);
    attributes.id = this.id;

    var self = this;
	$.each(attributes, function(key, value) {
		if (self.get(key) instanceof Parse.File) {
			attributes[key] = {'url': value.url()};
		}
		else if (_(value.toJSON).isFunction() || _.isArray(value)) {
			attributes[key] = value.toFullJSON();
		}
	});

	return attributes;
}

Array.prototype.toFullJSON = function() {
	return _.map(this, function(elem) {
		if (_.isFunction(elem.toFullJSON))
			return elem.toFullJSON();
		else
			return elem;
	});
}

var Organization = Parse.Object.extend("User");

var OrganizationCollection = Parse.Collection.extend({
	model: Organization,
	query: (new Parse.Query(Organization)).equalTo("is_organization", true)
});

var Project = Parse.Object.extend('project');

var ProjectCollection = Parse.Collection.extend({
	model: Project
});

var Event = Parse.Object.extend('event');

var EventCollection = Parse.Collection.extend({
	model: Event
});

var Category = Parse.Object.extend('category');

var AppRouter = Parse.Router.extend({
	/*initialize: function() {		//does not work because parse.js is based on an earlier version of Backbone
		var self = this;
		this.on('route', function() {
			if (self.contentView) self.contentView.remove();
		});
	},*/
	routes: {
		'': 'home',
		'olusumlar': 'organizations',
		'olusumlar/kategori/:categoryId': 'organizationsByCategory',
		'olusumlar/ara/:text': 'organizationsByTextSearch',
		'olusum/:id': 'organization',
		'olusum/:id/duzenle': 'editOrganization',
		'ara': 'search',
		'ara/:text': 'search',
		'etkinlikler': 'events',
		'etkinlikler/ara/:text': 'eventsByTextSearch',
		'etkinlik/yeni': 'newEvent',
		'etkinlik/:id': 'event',
		'etkinlik/:id/duzenle': 'editEvent',
		'projeler': 'projects',
		'projeler/ara/:text': 'projectsByTextSearch',
		'proje/yeni': 'newProject',
		'proje/:id': 'project',
		'proje/:id/duzenle': 'editProject',
		'*path': 'defaultRoute'
	},
	home: function() {
		if (this.contentView) this.contentView.remove();
		this.contentView = new HomeView();
	},
	organizations: function() {
		if (!this.contentView) {
			this.contentView = new OrganizationsView();
		}
		else if (!(this.contentView instanceof OrganizationsView)) {
			this.contentView.remove();
			this.contentView = new OrganizationsView();
		}
		this.contentView.listAll();
	},
	organizationsByCategory: function(categoryId) {
		if (!this.contentView) {
			this.contentView = new OrganizationsView();
		}
		else if (!(this.contentView instanceof OrganizationsView)) {
			this.contentView.remove();
			this.contentView = new OrganizationsView();
		}
		this.contentView.listByCategory(categoryId);
	},
	organizationsByTextSearch: function(text) {
		if (!this.contentView) {
			this.contentView = new OrganizationsView();
		}
		else if (!(this.contentView instanceof OrganizationsView)) {
			this.contentView.remove();
			this.contentView = new OrganizationsView();
		}
		this.contentView.listBySearch(text);
	},
	organization: function(id) {
		if (this.contentView) this.contentView.remove();
		this.contentView = new OrganizationView({id: id});
	},
	editOrganization: function(id) {
		if (this.contentView) this.contentView.remove();
		this.contentView = new EditOrganizationView({id: id});
	},
	search: function(text) {
		if (this.contentView) this.contentView.remove();
		this.contentView = this.searchView = new SearchView();
		if (text) this.searchView.search(text);
	},
	events: function() {
		if (this.contentView) this.contentView.remove();
		this.contentView = this.eventsView = new EventsView();
		this.eventsView.listAll();
	},
	eventsByTextSearch: function(text) {
		if (this.contentView) this.contentView.remove();
		this.contentView = this.eventsView = new EventsView();
		this.eventsView.listBySearch(text);
	},
	newEvent: function() {
		if (this.contentView) this.contentView.remove();
		this.contentView = new CreateEditEventView();
	},
	editEvent: function(id) {
		if (this.contentView) this.contentView.remove();
		this.contentView = new CreateEditEventView({id: id});
	},
	event: function(id) {
		if (this.contentView) this.contentView.remove();
		this.contentView = new EventView({id: id});
	},
	projects: function() {
		if (this.contentView) this.contentView.remove();
		this.contentView = this.projsView = new ProjectsView();
		this.projsView.listAll();
	},
	projectsByTextSearch: function(text) {
		if (this.contentView) this.contentView.remove();
		this.contentView = this.projsView = new ProjectsView();
		this.projsView.listBySearch(text);
	},
	newProject: function() {
		if (this.contentView) this.contentView.remove();
		this.contentView = new CreateEditProjectView();
	},
	editProject: function(id) {
		if (this.contentView) this.contentView.remove();
		this.contentView = new CreateEditProjectView({id: id});
	},
	project: function(id) {
		if (this.contentView) this.contentView.remove();
		this.contentView = new ProjectView({id: id});
	},
	defaultRoute: function(path) {
		if (this.contentView) this.contentView.remove();
		page.loadPage(path);
		page.render();
	}
});

var ContentView = Parse.View.extend({
	el: '#content-body',
	remove: function() {
		this.undelegateEvents();
		this.$el.empty();
		//this.stopListening();
		return this;
	}
});

var HomeView = ContentView.extend({
	events: {
		'click #btnSearch': 'searchButtonClicked',
		'keypress #inpSearch': 'enterPressed'
	},
	searchButtonClicked: function() {
		var text = this.$el.find('#inpSearch').val();
		if (text) window.location.hash = 'ara/' + text;
	},
	enterPressed: function(e) {
		if (e.which === 13) {
			var text = this.$el.find('#inpSearch').val();
			if (text) window.location.hash = 'ara/' + text;
		}
	},
	initialize: function() {
		this.render();
		new HomeOrganizationsView();
		new HomeProjectsView();
		new HomeEventsView();
	},
	template: Handlebars.compile($('#tmplHome').html()),
	render: function() {
		this.$el.html(this.template());
		page.updateNav('home');
		page.setupElements();
	}
});

var HomeOrganizationsView = Parse.View.extend({
	el: '#divHomeOrgs',
	initialize: function() {
		var self = this;
		Parse.Cloud.run('organization_search', {}, {
			success: function(organizations) {
				self.organizations = organizations;
				self.render();
			}
		});
	},
	template: Handlebars.compile($('#tmplHomeOrganizations').html()),
	render: function() {
		this.$el.html(this.template({orgs: this.organizations.toFullJSON()}));
	}
});

var HomeProjectsView = Parse.View.extend({
	model: new ProjectCollection(),
	el: '#divHomeProjs',
	initialize: function() {
		this.model.bind('reset', this.render, this);
		this.model.query = (new Parse.Query(Project)).descending('createdAt').limit(3);
		this.model.fetch();
	},
	template: Handlebars.compile($('#tmplHomeProjects').html()),
	render: function() {
		this.$el.html(this.template({projs: this.model.toJSON()}));
		return this;
	}
});

var HomeEventsView = Parse.View.extend({
	model: new EventCollection(),
	el: '#divHomeEvents',
	initialize: function() {
		this.model.bind('reset', this.render, this);
		this.model.query = (new Parse.Query(Event)).descending('createdAt').limit(3);
		this.model.fetch();
	},
	template: Handlebars.compile($('#tmplHomeEvents').html()),
	render: function() {
		this.$el.html(this.template({events: this.model.toJSON()}));
		return this;
	}
});

var SearchView = ContentView.extend({
	events: {
		'click #btnSearch': 'searchButtonClicked',
		'keypress #inpSearch': 'enterPressed'
	},
	searchButtonClicked: function() {
		var text = this.$el.find('#inpSearch').val();
		if (text) window.location.hash = 'ara/' + text;
	},
	enterPressed: function(e) {
		if (e.which === 13) {
			var text = this.$el.find('#inpSearch').val();
			if (text) window.location.hash = 'ara/' + text;
		}
	},
	search: function(text) {
		this.$el.find('#inpSearch').val(text);
		this.orgsView.search(text);
		this.projsView.search(text);
		this.eventsView.search(text);
	},
	initialize: function() {
		this.render();
		this.orgsView = new SearchOrganizationsView();
		this.projsView = new SearchProjectsView();
		this.eventsView = new SearchEventsView();
	},
	template: Handlebars.compile($('#tmplSearch').html()),
	render: function() {
		this.$el.html(this.template());
		page.updateNav('home');
		page.setupElements();
		return this;
	}
});

var SearchOrganizationsView = Parse.View.extend({
	el: '#ulOrgs',
	search: function(text) {
		var self = this;
		this.$el.addClass('loading');
		Parse.Cloud.run('organization_search', {text: text}, {
			success: function(organizations) {
				self.organizations = organizations;
				self.render();
			}
		});
	},
	template: Handlebars.compile($('#tmplSearchOrganizations').html()),
	render: function() {
		this.$el.removeClass('loading');
		this.$el.html(this.template({orgs: this.organizations.toFullJSON()}));
	}
});

var SearchProjectsView = Parse.View.extend({
	model: new ProjectCollection(),
	el: '#ulProjs',
	search: function(text) {
		this.$el.addClass('loading');
		this.model.query = Parse.Query.or(
			(new Parse.Query(Project)).matches("title__tr", new RegExp(text, 'i'))
			, (new Parse.Query(Project)).matches("description__tr", new RegExp(text, 'i'))
		);
		this.model.fetch();
	},
	initialize: function() {
		this.model.bind('reset', this.render, this);
	},
	template: Handlebars.compile($('#tmplSearchProjects').html()),
	render: function() {
		this.$el.removeClass('loading');
		this.$el.html(this.template({projs: this.model.toJSON()}));
		return this;
	}
});

var SearchEventsView = Parse.View.extend({
	model: new EventCollection(),
	el: '#ulEvents',
	search: function(text) {
		this.model.query = Parse.Query.or(
			(new Parse.Query(Event)).matches("title__tr", new RegExp(text, 'i'))
			, (new Parse.Query(Event)).matches("description__tr", new RegExp(text, 'i'))
		);
		this.model.fetch();
	},
	initialize: function() {
		this.model.bind('reset', this.render, this);
	},
	template: Handlebars.compile($('#tmplSearchEvents').html()),
	render: function() {
		this.$el.removeClass('loading');
		this.$el.html(this.template({events: this.model.toJSON()}));
		return this;
	}
});

var OrganizationsView = ContentView.extend({
	events: {
		'click #btnSearch': 'searchButtonClicked',
		'keypress #inpSearch': 'enterPressed'
	},
	searchButtonClicked: function() {
		var text = this.$el.find('#inpSearch').val();
		if (text) window.location.hash = 'olusumlar/ara/' + text;
	},
	enterPressed: function(e) {
		if (e.which === 13) {
			var text = this.$el.find('#inpSearch').val();
			if (text) window.location.hash = 'olusumlar/ara/' + text;
		}
	},
	listAll: function() {
		this.$el.find('#inpSearch').val('');

		this.listCategoriesView.listAll();
		this.listOrgsView.listAll();
	},
	listByCategory: function(categoryId) {
		this.$el.find('#inpSearch').val('');

		this.listCategoriesView.listByCategory(categoryId);
		this.listOrgsView.listByCategory(categoryId);
	},
	listBySearch: function(text) {
		this.$el.find('#inpSearch').val(text);

		this.listCategoriesView.listBySearch(text);
		this.listOrgsView.listBySearch(text);
	},
	initialize: function() {
		this.render();
		this.listCategoriesView = new ListCategoriesView();
		this.listOrgsView = new ListOrganizationsView();
	},
	template: Handlebars.compile($('#tmplOrganizations').html()),
	render: function() {
		page.updateNav('olusumlar');
		this.$el.html(this.template());
		return this;
	}
});

var ListOrganizationsView = Parse.View.extend({
	el: '#ulListOrgs',
	listAll: function() {
		var self = this;
		this.$el.addClass('loading');
		Parse.Cloud.run('organization_search', {}, {
			success: function(organizations) {
				self.organizations = organizations;
				self.render();
			}
		});
	},
	listByCategory: function(categoryId) {
		var self = this;
		this.$el.addClass('loading');
		Parse.Cloud.run('organization_search', {categoryId: categoryId}, {
			success: function(organizations) {
				self.organizations = organizations;
				self.render();
			}
		});
	},
	listBySearch: function(text) {
		var self = this;
		this.$el.addClass('loading');
		Parse.Cloud.run('organization_search', {text: text}, {
			success: function(organizations) {
				self.organizations = organizations;
				self.render();
			}
		});
	},
	template: Handlebars.compile($('#tmplListOrganizations').html()),
	render: function() {
		this.$el.html(this.template({organizations: this.organizations.toFullJSON()}));
		this.$el.removeClass('loading');
	}
});

var ListCategoriesView = Parse.View.extend({
	el: '#ulCategories',
	listAll: function() {
		var self = this;
		if (!self.categories)  {
			setTimeout(function() { self.listAll() }, 100);
		}
		this.$el.find('.category').removeClass('selected');
		this.$el.find('.allCategories').addClass('selected');
	},
	listByCategory: function(categoryId) {
		var self = this;
		if (!self.categories)  {
			setTimeout(function() { self.listByCategory(categoryId) }, 100);
		}
		this.$el.find('.category').removeClass('selected');
		this.$el.find('.category[data-id = "' + categoryId + '"]').addClass('selected');
	},
	listBySearch: function(text) {
		var self = this;
		if (!self.categories)  {
			setTimeout(function() { self.listBySearch(text) }, 100);
		}
		this.$el.find('.category').removeClass('selected');
		this.$el.find('.allCategories').addClass('selected');
	},
	initialize: function() {
		var self = this;
		Parse.Cloud.run('category_list', {}, {
			success: function(categories) {
				self.categories = categories;
				self.render();
			}
		});
	},
	template: Handlebars.compile($('#tmplListCategories').html()),
	render: function() {
		var arrCategories = [];
		for (var i = 0; i < this.categories.length; i++) {
			arrCategories.push(this.categories[i].toJSON());
		}
		this.$el.html(this.template({categories: arrCategories}));
		return this;
	}
});

var OrganizationView = ContentView.extend({
	initialize: function() {
		var self = this;
		Parse.Cloud.run('organization_get', {'organization_id': self.options.id}, {
			success: function(organization) {
				self.organization = organization;
				self.render();
				new OrganizationProjectsView({creatorId: self.options.id});
				new OrganizationEventsView({creatorId: self.options.id});
			}
		});
	},
	template: Handlebars.compile($('#tmplOrganization').html()),
	render: function() {
		page.updateNav('olusumlar');
		this.$el.html(this.template({
			org: this.organization.toJSON()
			, isAdmin: Parse.User.current() && (this.organization.id == Parse.User.current().id)
		}));
		return this;
	}
});

var OrganizationProjectsView  = Parse.View.extend({
	model: new ProjectCollection(),
	el: '#ulOrgProjs',
	initialize: function() {
		this.model.bind('reset', this.render, this);

		var creator = new Parse.User();
		creator.id = this.options.creatorId;

		this.model.query = (new Parse.Query(Project))
			.equalTo("creator", creator);
		this.model.fetch();
	},
	template: Handlebars.compile($('#tmplOrganizationProjects').html()),
	render: function() {
		this.$el.removeClass('loading');
		this.$el.html(this.template({projs: this.model.toJSON()}));
		return this;
	}
});

var OrganizationEventsView = Parse.View.extend({
	model: new EventCollection(),
	el: '#ulOrgEvents',
	initialize: function() {
		this.model.bind('reset', this.render, this);

		this.$el.addClass('loading');

		var creator = new Parse.User();
		creator.id = this.options.creatorId;

		this.model.query = (new Parse.Query(Event))
			.equalTo("creator", creator);
		this.model.fetch();
	},
	template: Handlebars.compile($('#tmplOrganizationEvents').html()),
	render: function() {
		this.$el.removeClass('loading');
		this.$el.html(this.template({events: this.model.toJSON()}));
		return this;
	}
});

var EditOrganizationView = ContentView.extend({
	events: {
		'change input[type="text"]':  'textInputChanged',
		'change input[type="email"]':  'textInputChanged',
		'change #about': 'aboutChanged',
		'change input[type="checkbox"]': 'categoryChanged',
		'click #btnSaveOrg': 'saveOrganization',
		'change #picture': 'pictureChanged'
	},
	changedAttributes: {},
	textInputChanged: function(e) {
		var changed = e.currentTarget;
		var value = $(e.currentTarget).val();

		this.changedAttributes[changed.id] = value;
    },
    pictureChanged: function (e) {
    	var self = this;

		var files = e.target.files || e.dataTransfer.files;
		file = files[0];

		var name = "logo.jpg";
		var parseFile = new Parse.File(name, file);
		parseFile.save().then(
			function() {
				self.changedAttributes.picture = parseFile;
			},
			function(error) {
				alert('An error occurred.');
  			}
  		);
    },
    aboutChanged: function() {
    	this.changedAttributes.about = this.$el.find('#about').val();
    },
	categoryChanged: function (e) {
		var checkBox = e.currentTarget;

		var categories = this.model.get('categories');
		if (!categories) categories = [];

		if (checkBox.checked) categories.push(new Category({id: checkBox.value}));
		else {
			for (var i = 0; i < categories.length; i++)
				if (categories[i].id == checkBox.value) {
					categories.splice(i, 1);
					break;
				}
		}

		this.changedAttributes.categories = _.map(categories, function(category) {
			return {__type: 'Pointer', className: 'category', objectId: category.id};
		});
	},
    saveOrganization: function() {
    	var self = this;

    	Parse.Cloud.run('organization_edit', self.changedAttributes, {
    		success: function() {
    			window.location.hash = 'olusum/' + self.model.id;
    		},
    		error: function() {
    			alert('Bir problem oluştu. Oluşum kaydedilemedi.');
			}
    	});
    },
    initialize: function() {
    	var self = this;
    	var promises = [];

		promises.push(Parse.Cloud.run('organization_get', {organization_id: self.options.id}));
		promises.push(Parse.Cloud.run('category_list', {}));

		Parse.Promise.when(promises).then(function(org, categories) {
			self.model = org;
			self.allCategories = categories;
			self.render();
		});
	},
	template: Handlebars.compile($('#tmplEditOrganization').html()),
	render: function() {
		page.updateNav('olusumlar');

		var orgJSON = this.model.toJSON();
		if (!orgJSON.categories) orgJSON.categories = [];

		var categoriesJSON = [];
		for (var i = 0; i < this.allCategories.length; i++) {
			var found = false;
			for (var j = 0; j < orgJSON.categories.length; j++) {
				if (orgJSON.categories[j].objectId == this.allCategories[i].id) {
					found = true;
					break;
				}
			}

			categoriesJSON.push({
				objectId: this.allCategories[i].id,
				title: this.allCategories[i].get('title'),
				selected: found
			});
		}

		orgJSON.categories = categoriesJSON;
		this.$el.html(this.template({organization: orgJSON}));
	}
});

var EventsView = ContentView.extend({
	events: {
		'click #btnSearch': 'searchButtonClicked',
		'keypress #inpSearch': 'enterPressed'
	},
	searchButtonClicked: function() {
		var text = this.$el.find('#inpSearch').val();
		if (text) window.location.hash = 'etkinlikler/ara/' + text;
	},
	enterPressed: function(e) {
		if (e.which === 13) {
			var text = this.$el.find('#inpSearch').val();
			if (text) window.location.hash = 'etkinlikler/ara/' + text;
		}
	},
	listAll: function() {
		this.$el.find('#inpSearch').val('');
		this.listEventsView.listAll();
	},
	listBySearch: function(text) {
		this.$el.find('#inpSearch').val(text);
		this.listEventsView.listBySearch(text);
	},
	initialize: function() {
		this.render();
		this.listEventsView = new ListEventsView();
	},
	template: Handlebars.compile($('#tmplEvents').html()),
	render: function() {
		page.updateNav('etkinlikler');
		this.$el.html(this.template({}));
		return this;
	}
});

var ListEventsView = Parse.View.extend({
	model: new EventCollection(),

	el: '#ulListEvents',

	listAll: function() {
		this.$el.addClass('loading');
		this.model.query = new Parse.Query(Event).descending('createdAt');
		this.model.fetch();
	},

	listBySearch: function(text) {
		this.$el.addClass('loading');
		this.model.query = Parse.Query.or(
			(new Parse.Query(Event)).matches("title__tr", new RegExp(text, 'i'))
			, (new Parse.Query(Event)).matches("description__tr", new RegExp(text, 'i'))
		).descending('createdAt');
		this.model.fetch();
	},

	initialize: function() {
		this.model.bind('reset', this.render, this);
	},

	template: Handlebars.compile($('#tmplListEvents').html()),

	render: function() {
		this.$el.html(this.template({events: this.model.toJSON()}));
		this.$el.removeClass('loading');
		return this;
	}
});

var CreateEditEventView = ContentView.extend({
	model: new Event(),
	events: {
		'change input.eventField':  'eventFieldChanged',
		'change #description__tr': 'descriptionChanged',
		'change #picture': 'pictureChanged',
		'click #btnSaveEvent': 'saveEvent',
		'click .remove': 'removeRequirement',
		'click .add-row a': 'addRequirementRow',
		'change .needField': 'needFieldChanged'
	},
	eventFieldChanged: function(e) {
		var changed = e.currentTarget;
		var value = $(e.currentTarget).val();

		var obj = {};
		obj[changed.id] = value;
		this.model.set(obj);
    },
	descriptionChanged: function() {
		this.model.set({'description__tr': this.$el.find('#description__tr').val()});
	},
    pictureChanged: function (e) {
		var files = e.target.files || e.dataTransfer.files;
		file = files[0];

		var model = this.model;

		var name = "logo.jpg";
		var parseFile = new Parse.File(name, file);
		parseFile.save().then(function() {
			model.set("picture", parseFile);
		}, function(error) {
			console.log('Error occurred.>');
			console.log(error);
  		});
    },
	saveEvent: function() {
		var self = this;

		var needs = self.model.get('needs__tr');
		for (var i = 0; i < needs.length; i++) {
			if (
				needs[i].deleted || 
				!(needs[i].need || needs[i].amount || needs[i].description)
			) {
				needs.splice(i, 1);
				i--;
			}
		}

		self.model.set('creator', Parse.User.current());
    	self.model.save(null, {
			success: function() {
				window.location.hash = 'etkinlik/' + self.model.id;
			},
			error: function() {
				alert('Bir problem oluştu. Etkinlik kaydedilemedi.');
			}
		});
	},
	needFieldChanged: function(e) {
		var $changed = $(e.currentTarget);
		var fieldName;
		if ($changed.hasClass('what')) fieldName = 'need'
		else if ($changed.hasClass('how-many')) fieldName = 'amount'
		else if ($changed.hasClass('description')) fieldName = 'description'
		else {
			console.log('Error>Unknown class name.');
			return;
		}
		
		var needs = this.model.get('needs__tr');
		needs[$changed.parent().data('index')][fieldName] = $changed.val();
	},
	removeRequirement: function(e) {
		var $need = $(e.currentTarget).parent();

		var needs = this.model.get('needs__tr');
		needs[$need.data('index')].deleted = true;

		$need.remove();

		console.log(needs);

		return false;
	},
	addRequirementRow: function() {
		var $newNeedRow = $('.requirements .item-template').clone();
		$newNeedRow.insertBefore('.requirements .add-row').removeClass('item-template').slideDown(100);

		var needs = this.model.get('needs__tr');
		needs.push({});

		$newNeedRow.data('index', needs.length - 1);

		return false;
	},
	initialize: function() {
		var self = this;
		if (self.options.id) {
			self.model.id = self.options.id;
			self.model.fetch({
				success: function() {
					self.render();
				}
			});
		}
		else this.render();
	},
	template: Handlebars.compile($('#tmplCreateEditEvent').html()),
	render: function() {
		page.updateNav('etkinlikler');
		this.$el.html(this.template({event: this.model.toJSON()}));
		return this;
	}
});

var EventView = ContentView.extend({
	events: {
		'click #btnJoin': 'joinButtonClicked',
		'click #btnUndoJoin': 'undoJoinButtonClicked'
	},
	joinButtonClicked: function(e) {
		var self = this;
		Parse.Cloud.run('event_attend', {"event_id": self.options.id, "should_attend": "true"}, {
			success: function(result) {
				self.$el.find('#spanJoinCount').text(result.join_count);
				self.$el.find('#btnJoin').toggleClass('invisible');
				self.$el.find('#btnUndoJoin').toggleClass('invisible');
				self.orgJoinersView.render();
				self.eventJoinersView.render();
			},
			error: function(result) {
				$('.login').click();
			}
		});
	},
	undoJoinButtonClicked: function() {
		var self = this;
		Parse.Cloud.run('event_attend', {"event_id": self.options.id, "should_attend": "false"}, {
			success: function(result) {
				self.$el.find('#spanJoinCount').text(result.join_count);
				self.$el.find('#btnJoin').toggleClass('invisible');
				self.$el.find('#btnUndoJoin').toggleClass('invisible');
				self.orgJoinersView.render();
				self.eventJoinersView.render();
			}
		});
	},
	initialize: function() {
		var self = this;

		Parse.Cloud.run('event_get', {'event_id': self.options.id}, {
			success: function(event) {
				self.event = event;
				self.render();
				self.orgJoinersView = new EventOrganizationJoinersView({id: self.options.id});
				self.orgJoinersView.render();
				self.eventJoinersView = new EventUserJoinersView({id: self.options.id});
				self.eventJoinersView.render();
			}
		});

		/*$.ajax({
			url: $.ajaxSetup()['url'] + 'event_get',
			data: JSON.stringify({
				"_SessionToken": Parse.User.current() ? Parse.User.current()._sessionToken : '',	//TODO: get rid of this
				"event_id": self.options.id
			}),
			success: function(data) {
				self.event = data.result;
				self.render();
				self.orgJoinersView = new EventOrganizationJoinersView({id: self.options.id});
				self.orgJoinersView.render();
				self.eventJoinersView = new EventUserJoinersView({id: self.options.id});
				self.eventJoinersView.render();
			}
		});*/
	},
	template: Handlebars.compile($('#tmplEvent').html()),
	render: function() {
		page.updateNav('etkinlikler');
		this.$el.html(this.template({
			event: this.event.toFullJSON()
			, isAdmin: Parse.User.current() && (this.event.get('creator').id == Parse.User.current().id)
		}));
	}
});

var EventOrganizationJoinersView = Parse.View.extend({
	el: '#liOrganizationJoiners',
	template: Handlebars.compile($('#tmplOrganizationJoiners').html()),
	render: function() {
		var self = this;
		$.ajax({
			url: $.ajaxSetup()['url'] + 'event_get_joiners_org',
			data: '{"event_id":"' +  self.options.id + '"}',
			success: function(data) {
				self.$el.html(self.template({
					joiners: data.result.joiners_org
				}));
			}
		});
	}
});

var EventUserJoinersView = Parse.View.extend({
	el: '#liUserJoiners',
	template: Handlebars.compile($('#tmplUserJoiners').html()),
	render: function() {
		var self = this;
		$.ajax({
			url: $.ajaxSetup()['url'] + 'event_get_joiners',
			data: '{"event_id":"' +  self.options.id + '"}',
			success: function(data) {
				self.$el.html(self.template({
					joiners: data.result.joiners
				}));
			}
		});
	}
});

var CreateEditProjectView = ContentView.extend({
	model: new Project(),
	events: {
		'change input[type="text"]':  'textInputChanged',
		'change #description__tr': 'descriptionChanged',
		'change #picture': 'pictureChanged',
		'click #btnSaveProj': 'saveProject',
		'click .remove': 'removeRequirement',
		'click .add-row a': 'addRequirementRow'
	},
	textInputChanged: function(e) {
		var changed = e.currentTarget;
		var value = $(e.currentTarget).val();

		var obj = {};
		obj[changed.id] = value;
		this.model.set(obj);
    },
	descriptionChanged: function() {
		this.model.set({'description__tr': this.$el.find('#description__tr').val()});
	},
    pictureChanged: function (e) {
		var files = e.target.files || e.dataTransfer.files;
		file = files[0];

		var model = this.model;

		var name = "logo.jpg";
		var parseFile = new Parse.File(name, file);
		parseFile.save().then(function() {
			model.set("picture", parseFile);
		}, function(error) {
			console.log('Error occurred.>');
			console.log(error);
  		});
    },
	saveProject: function() {
		var self = this;
		self.model.set('creator', Parse.User.current());
    	self.model.save(null,
	    	{
	    		success: function(model, response) {
	    			console.log('Save success. model>' + model + ', response>' + response);
	    			window.location.hash = 'proje/' + self.model.id;
	    		},
	    		error: function (model, response) {
        			console.log("Save error. response>");
        			console.log(response);
        			alert('Bir problem oluştu. Proje kaydedilemedi.');
    			}
	    	}
    	);
	},
	removeRequirement: function(e) {
		var requirement = $(e.currentTarget).parent();

		$(requirement).hide(100).addClass('deleted');
		// TODO: remove saved requirement from database

		return false;
	},
	addRequirementRow: function() {
		$('.requirements .item-template').clone().insertBefore('.requirements .add-row').removeClass('item-template').slideDown(100);

		return false;
	},
	initialize: function() {
		var self = this;
		if (self.options.id) {
			self.model.id = self.options.id;
			self.model.fetch({
				success: function() {
					self.render();
				}
			});
		}
		else this.render();
	},
	template: Handlebars.compile($('#tmplCreateEditProject').html()),
	render: function() {
		page.updateNav('projeler');
		this.$el.html(this.template({proj: this.model.toJSON()}));
		return this;
	}
});

var ProjectsView = ContentView.extend({
	events: {
		'click #btnSearch': 'searchButtonClicked',
		'keypress #inpSearch': 'enterPressed'
	},
	searchButtonClicked: function() {
		var text = this.$el.find('#inpSearch').val();
		if (text) window.location.hash = 'projeler/ara/' + text;
	},
	enterPressed: function(e) {
		if (e.which === 13) {
			var text = this.$el.find('#inpSearch').val();
			if (text) window.location.hash = 'projeler/ara/' + text;
		}
	},
	listAll: function() {
		this.$el.find('#inpSearch').val('');
		this.listProjsView.listAll();
	},
	listBySearch: function(text) {
		this.$el.find('#inpSearch').val(text);
		this.listProjsView.listBySearch(text);
	},
	initialize: function() {
		this.render();
		this.listProjsView = new ListProjectsView();
	},
	template: Handlebars.compile($('#tmplProjects').html()),
	render: function() {
		page.updateNav('projeler');
		this.$el.html(this.template({}));
		return this;
	}
});

var ListProjectsView = Parse.View.extend({
	model: new ProjectCollection(),
	el: '#ulListProjs',
	listAll: function() {
		this.$el.addClass('loading');
		this.model.fetch();
	},
	listBySearch: function(text) {
		this.$el.addClass('loading');
		this.model.query = 	Parse.Query.or(
			(new Parse.Query(Project)).matches("title__tr", new RegExp(text, 'i'))
			, (new Parse.Query(Project)).matches("description__tr", new RegExp(text, 'i'))
		);
		this.model.fetch();
	},
	initialize: function() {
		this.model.bind('reset', this.render, this);
	},
	template: Handlebars.compile($('#tmplListProjs').html()),
	render: function() {
		this.$el.html(this.template({projs: this.model.toJSON()}));
		this.$el.removeClass('loading');
		return this;
	}
});

var ProjectView = ContentView.extend({
	events: {
		'click #btnEditProj': 'editButtonClicked'
	},
	editButtonClicked: function() {
		window.location.hash = '#proje/' + this.project.id + '/duzenle';
	},
	initialize: function() {
		var self = this;
		Parse.Cloud.run('project_get', {'project_id': self.options.id}, {
			success: function(project) {
				self.project = project;
				self.render();
			}
		});
	},
	template: Handlebars.compile($('#tmplProject').html()),
	render: function() {
		page.updateNav('projeler');	

		this.$el.html(this.template({
			project: this.project.toFullJSON()
			, isAdmin: Parse.User.current() && (this.project.get('creator').id == Parse.User.current().id)
		}));
	}
});

var UserView = Parse.View.extend({
	el: '#divUser',

	initialize: function() {
		this.render();
	},

	render: function() {
		if (this.subview) this.subview.remove();	//todo: there may be a memory leak here

		if (Parse.User.current()) this.subview = new LoggedInUserView();
		else this.subview = new AnonymousUserView();

		this.subview.parentView = this;
		this.$el.append(this.subview.el);

		return this;
	}
});

var AnonymousUserView = Parse.View.extend({
	events: {
        'click .login' : 'openLoginBox',
        'click #btnLogIn': 'logIn',
        'click .signup': 'openSignupBox',
        'click #signup-link': 'openSignupBox',
        'click #btnSignUpOrg': 'signUpOrg',
        'click #btnSignUpUser': 'signUpUser'
    },

	openLoginBox: function() {
		$('#signup-box').slideUp(0);
		$('#login-box').slideToggle(300);
		$('#main-overlay').fadeIn(300);
	},

	logIn: function() {
		var self = this;

		var email = $('#login-user-email').val();
		var password = $('#login-user-password').val();

		var buttonText = $('#btnLogIn').html('Yükleniyor...');

		Parse.User.logIn(email, password, {
			success: function(user) {
				self.parentView.render();
				window.location.hash = '';
				console.log('User logged in. User\'s email is>' + (user.emailVerified ? 'verified' : 'unverified'));
				$('#btnLogIn').html(buttonText);
				$('#main-overlay').fadeOut(100);
			},
			error: function(user, error) {
				$('#btnLogIn').html(buttonText);
				alert('Invalid username or password. Please try again.');
				console.log('logIn error. Message: ' + error.message);
			}
		});

		return false;
	},

    openSignupBox: function() {
		$('#login-box').slideUp(0);
		$('#signup-box').slideToggle(300);
		$('#main-overlay').fadeIn(300);
	},

	signUpOrg: function() {
		var self = this;

		var email = $('#signUpOrgEmail').val();
		var password = $('#signup-password').val();
		var name = $('#signUpOrganizationName').val();

		var newOrg = new Parse.User();
		newOrg.setUsername(email);
		newOrg.setEmail(email);
		newOrg.setPassword(password);
		newOrg.set({display_name: name});
		newOrg.set({is_organization: true});

		var buttonText = $('#btnSignUpOrg').html('Yükleniyor...');

		newOrg.signUp({   }, {
			success: function(user) {
				self.parentView.render();
				window.location.hash = 'olusum/' + user.id + '/duzenle';
				$('#btnSignUpOrg').html(buttonText);
				$('#main-overlay').fadeOut(100);
			},
			error: function(user, error) {
				$('#btnSignUpOrg').html(buttonText);
				alert('Sign-up unsuccessful. Error: ' + error.message);
				console.log('signUp error. Message: ' + error.message);
			}
		});
	},

	signUpUser: function() {
		var self = this;

		var email = $('#signup-user-email').val();
		var password = $('#signup-password').val();

		var newUser = new Parse.User();
		newUser.setUsername(email);
		newUser.setEmail(email);
		newUser.setPassword(password);

		var buttonText = $('#btnSignUpUser').html('Yükleniyor...');

		newUser.signUp({  }, {
			success: function(user) {
				self.parentView.render();
				window.location.hash = '';
				console.log('User signed up');
				$('#btnLogIn').html(buttonText);
				$('#main-overlay').fadeOut(100);
			},
			error: function(user, error) {
				alert('Sign-up unsuccessful. Error: ' + error.message);
				console.log('signUp error. Message: ' + error.message);
				$('#btnSignUpUser').html(buttonText);
			}
		});
	},

	initialize: function() {
		this.render();
	},

	template: Handlebars.compile($('#tmplAnonymousUser').html()),

	render: function() {
		this.$el.html(this.template());
		return this;
	}
});

var LoggedInUserView = Parse.View.extend({
	events: {
        'click #linkLogOut': 'logOut'
    },

    logOut: function() {
		Parse.User.logOut();
		this.parentView.render();
		window.location.hash = '';
	},

	initialize: function() {
		this.render();
	},

	template: Handlebars.compile($('#tmplLoggedInUser').html()),

	render: function() {
		this.$el.html(this.template({username: Parse.User.current().getUsername()}));
		return this;
	}
});

$(function() {
	Parse.initialize(appId, jsKey);

	$.ajaxSetup({
		type: 'POST',
		url: 'https://api.parse.com/1/functions/',
		headers: {
			"X-Parse-Application-Id": appId,
	  		"X-Parse-REST-API-Key": restAPIKey
	  	},
	  	contentType: 'application/json',
	  	dataType: 'json',
	  	processData: false
	});

	var router = new AppRouter();

	Parse.history.start();

	var userView = new UserView({});
});

})();