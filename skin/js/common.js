//(function (window, document, undefined){
"use strict";
var Page = {};
var DOM = {};
var Utilities = {};
var Home = {};
var Pages = {
	home: {}
};

DOM.$contentBody = $('#content-body');
DOM.$mainNav = $('.main-nav');

Utilities = function () {
	this.log = function ( text ){
		if( typeof console != 'undefined' )
		{
			console.log(text);
		}
	};

	this.removePX = function (str) {
		return str.substr(0, str.length - 2);
	};

	this.preloadImages = function (container){
		if ( !$(container).length )
		{
			return true;
		}

		var nCompleted = 0;
		$('img', $(container)).each(function (index, node){
			if( $(node)[0].complete )
			{
				nCompleted++;
			}
		});

		return nCompleted == $('img', $(container)).length;
	};

	this.deferImageLoad = function () {
		$('img.lazy').each(function (index, node){
			var src = $(node).attr('data-original');
			$(node).attr('src', src);
		})
	};

	this.areFontsLoaded = function () {
		if ($('html').hasClass('wf-active'))
		{
			page.fontsLoaded = 1;
		}

		return page.fontsLoaded;
	};
}

var Util = new Utilities();

Home = function() {
	var self = this;

	self.positionElements = function (w, h) {
		if ($('#intro').length) {
			log($('.bg').height())
			// $('#intro').css('marginTop', -$('.bg').height() + 120);
			$('.boxes').css('marginTop', $('.bg').height() - $('#intro').height() - 120 + $('.caption').outerHeight() - $('.boxes').height());
		}
	};

	self.animateContent = function () {
        $('#test').css('opacity', .5);
	};

	self.setup = function() {
		$('.box .inner').hover(function () {
			$(this).find('.title').addClass('visible');
			$(this).find('.caption').css('top', $('.box .inner').height() - 68 + 6).animate({top: '6px'}, 600);
			$(this).find('.desc').slideDown(500);
			$(this).find('.more').delay(400).fadeIn(500);
		}, function () {
			$(this).find('.more').fadeOut(300);
			$(this).find('.desc').slideUp(400);
			$(this).find('.caption').animate({top: $('.box .inner').height() - 68 + 6}, 600, function () {
				$(this).css('top', 'auto');
			});
			$(this).find('.title').removeClass('visible');
		});
	};
}

Pages.home = new Home();

Page = function (){
	var self = this;

	self.render = function () {
		self.animateSite();
		self.setup();

		self.positionElements();
		$(window).resize(function (){
			self.positionElements();
		})
	};

	self.positionElements = function () {
		var w = $(window).width(),
			h = $(window).height();

		if (typeof Pages[self.mainPage] != 'undefined' && typeof Pages[self.mainPage].positionElements === 'function') {
        	Pages[self.mainPage].positionElements(w, h);
        }
	};

	self.animateSite = function () {
		var callback = function () {
			self.animateReady = 1;
		}

		if ($('.preload_header').length)
        {
            $('.preload_header').waitForImages({
			    finished: function () {
			        callback();
			    },
			    each: function (loaded, count, success) {
			    },
			    waitForAll: true
			});
		}
		else
		{
			callback();
		}
	};

	self.loadPage = function ( newPage )	{
		var parts = newPage.split('/');
		self.mainPage = parts[0] ? parts[0] : self.mainPage;
		self.subPage = parts[1] ? parts[1] : '';

		$('body').attr({'data-page': self.mainPage, 'data-subpage': self.subPage});

		DOM.$contentBody.animate( {opacity:1}, self.animationDuration, function () {
			DOM.$contentBody.html('').css({opacity:1}).addClass('loading');
			$.ajax({
				url: STORE_URL + newPage,
				dataType: 'html',
				type: 'GET',
				// data: {main_page: self.mainPage, sub_page: self.subPage},
				success: function (response){
					self.pageHTML = response;
					self.animateContent();
					self.updateNav();
				},
				error: function (response){
					self.pageHTML = $('.error-not-found').clone().show();
					self.animateContent();
				}
			});
		});
	};

	self.animateContent = function () {
		var delay = 0;

		if( self.animateReady && Util.preloadImages($('.preload') ) )
		{
			$('#temp_html').html($.parseHTML(self.pageHTML));

            var callback = function () {
            	$('#temp_html').html('');
            	DOM.$contentBody.css({opacity:0}).removeClass('loading').html(self.pageHTML);

            	var title = $('title', DOM.$contentBody).html();
				if( title )
				{
					$('head title').html( title );
				}

				var desc = $('meta[name="description"]', DOM.$contentBody).attr('content');
				if( desc )
				{
					$('head meta[name="description"]').attr( 'content', desc );
				}

				var keywords = $('meta[name="keywords"]', DOM.$contentBody).attr('content');
				if( keywords )
				{
					$('head meta[name="keywords"]').attr( 'content', keywords );
				}

            	self.positionElements();

            	Util.deferImageLoad(3000);
				self.setupElements();

				if (typeof Pages[self.mainPage] != 'undefined' &&  typeof Pages[self.mainPage].setup === 'function') {
		        	Pages[self.mainPage].setup();
		        }

            	DOM.$contentBody.delay(delay).animate({opacity: 1}, self.animationDuration, function (){

					if (typeof Pages[self.mainPage] != 'undefined' && typeof Pages[self.mainPage].animateContent === 'function') {
			        	Pages[self.mainPage].animateContent();
			        }
				});
            }

            if ($('.preload', $('#temp_html')).length)
            {
	            $('.preload', $('#temp_html')).waitForImages({
				    finished: function () {
				        callback();
				    },
				    each: function (loaded, count, success) {
				    },
				    waitForAll: true
				});
			}
			else
			{
				callback();
			}
		}
		else
		{
			setTimeout( self.animateContent, 50 );
		}
	}

	self.updateNav = function (navLinkName) {
		var navUpdated = 0,
			navLink = '';

		if (!navLinkName) {
			navLinkName = self.mainPage;
		}

		if( !DOM.$mainNav.is(':visible') ) {
			setTimeout( self.updateNav, 50);
			return false;
		}

		if (navLinkName == 'proje') {
			navLinkName = 'projeler';
		}

		if (navLinkName == 'olusum') {
			navLinkName = 'olusumlar';
		}

		if (navLinkName == 'etkinlik') {
			navLinkName = 'etkinlikler';
		}

		navLink = DOM.$mainNav.find('a[data-page="' + navLinkName + '"]');

		if( navLink.length ) {
			DOM.$mainNav.find('a').removeClass('selected');
			navLink.addClass('selected');

			navUpdated = 1;
		}
		else {
			DOM.$mainNav.find('a').removeClass('selected');
		}
	};

	self.setup = function () {
		$('#main-overlay').on('click', function () {
			$('.header-popup:visible').fadeOut(self.animationDuration);
			$(this).delay(100).fadeOut(self.animationDuration);
		})

		$('[data-show-action]').unbind('click').live('click', function () {
			var scope = $($(this).attr('data-scope')),
				toShow = $(scope).find('[data-show="' + $(this).attr('data-show-action') + '"]');

			$(scope).find('[data-show]').hide();
			$(toShow).show();

			$(scope).find('[data-show-action]').addClass('disabled');
			$(this).removeClass('disabled');
			return false;
		})
	};

	self.setupElements = function () {
		var links = $('a, .inputbutton');
		for ( var i = 0; i < links.length; i++ ) {

			if ( /msie/i.test(navigator.userAgent) ) {
				links[i].onfocus = function () {
					this.blur();
				}
			}
		}


	};
}

Page.prototype = {
	mainPage: 'home',
	subPage: '',
	animateReady: 0,
	fontsLoaded: 0,
	pageHTML: '',
	animationDuration: 400
}

var page = new Page();
page.render();

function log(text){Util.log(text);}

//})(this, document);